# Lazio Breast Cancer Screening

#### Background and current status [9/21/18]:

In January 2018, we started a conversation around whether we could use machine learning to improved targeting of cancer screening tests in Lazio (Rome). The specific question is: *Can we identify patients who are likely to have a positive screening test (colonoscopy, mammography, etc)?*.

To do this, we'd like to link the tests to as much other data as possible from before the test, to predict who will test positive. This could include
  - Hospital Information System data (back to 2000)
  - Pharmaceutical information system
  - Ambulatory visits and procedures (within the public health system, not private).

We'd first like to answer an even more basic question: *What are the current screening patterns in Lazio, and is there scope for improvement, if we had better predictions on test results?*

To answer this question, we've done preliminary analysis of a dataset of screening events (mammograms), accompanied by a dataset of women diagnosed with cancer (who may or may not have been screened), which received in July 2018. This document presents the results of the analysis.

#### Summary of findings to date

1. *How many women are screened, but are not found to have cancer?*
  - Of the 594,848 screening mammograms performed (in 364,774 patients), 47,013 (37,806) were positive.
    - Of these positive screens, 3,518 (3,483) had a positive Stage 2, and 2,280 (2,263) had invasive tissue on surgery. There were also 512 patients whose surgery results were missing, but who appeared in the cancer registry.
    - Overall positive rate: 0.47% of screenings (0.76% of patients)
  - **This suggests a large opportunity to avoid screening in women who have a very low predicted likelihood of a positive test.**
    - Interestingly, an additional 4,066 screening exams (in 3,068 women) were negative (mostly at the mammography stage: 3079 [2356]), but eventually appeared in the cancer registry, suggesting we should not simply predict the results of testing.
1. *Are there women diagnosed with cancer who were never screened?* **Yes**. These fall into two groups:
    - Women invited to screen, who declined screening
      - Of the 1,709,580 screening invitations (sent to 783,338 patients), 65.2% were declined (1,114,732 invitations for 578,817 patients).
      - 15,394 of these invitations were sent to 8,111 patients who were eventually diagnosed with cancer.
        - Note: this includes (i) women who were later screened, just after a delay because they declined the invitation; and (ii) women who were never screened.
          - Median number of declined invitations before diagnosis: 1 (0-2 10-90th pctile range)
          - Median time from first declined invitation to diagnosis: 140 (43-331 10-90th pctile range)
      - Overall cancer rate among those who declined an invitation: 0.90% (1.03%).
      - **This suggests a large opportunity to pursue more aggressive screening in women who decline invitations, but may have a high predicted likelihood of a positive test.**
    - Women never invited to screen, because they did not meet screening criteria
      -  1,201 women were under 50 years old at the time they appeared in the cancer registry
      - **This suggests that there are some women who are not currently invited to screen, who would benefit from screening, if they were at high predicted risk.**


## 1. Analysis

### How are patients sampled for inclusion in this analysis?

- Woman between the ages of 50-69
- Resident in Lazio Region
- Invited to at least 1 screening during 2009-2015

### Dataset and the unit of analysis

Our primary analytic data frame consists of observations on:
- Screening mammograms ($`i`$)
    - mod file name: `screening_lazio.feather`
        - `ID_event`: event identifier
        - note that these are not unique to a specific event but rather a grouping. (164 duplicated `ID_event`)
        - see "Duplicated ID_event" section for more details. For now, we are keeping this, but may want to drop (?)
- For women ($`j`$)
    - mod file name: `dem_lazio.feather`
        - `ID_P`: personal identifier
        -  `dt_birth`: date of birth
        - `t0_age`: our defined age = `Invitation_date` - `dt_birth`
- On specific dates ($`t`$)
    - mod file name: `screening_lazio.feather`
        - `Invitation_date`: date of screening mammography (t0)

- Which can lead to a second stage round (consisting of several tests, $`D_{ijt}^2`$), if the first stage is positive
    - mod file name: `stage2_lazio.feather`
        - `stage2_start_date`: date of the first exam (primary)
        - `stage2_Mammo_Date`: 2nd stage Mammography date
        - `stage2_Eco_Date`: date of 2nd Stage Ultrasound
        - `stage2_Cito_Date`: date of 2nd Stage Citological Exam
        - `stage2_Biopsy_Date`: date of 2nd Stage Biopsy
        - `stage2_Rls_date`: date of 2nd stage Final Report

- Which can lead to a third stage round (consisting of surgery, $`D_{ijt}^3`$)
    - mod file name: `stage3_lazio.feather`
        - `Surgery_date`: date of surgery
        - `Histo_date`: date of Histological Report
        - `Surgery_Results_date`: date of 3rd stage Final Report

We also have a secondary dataset `BREAST_CR.txt` indexing women diagnosed with breast cancer ($`Y_{jt}`$)
- mod file name: `breast_cancer_lazio.feather`
  - `dtinc`: date of diagnosis

### Sample exclusions and restrictions

Both datasets start in 2009 and run to the end of 2015. But we need to apply some restrictions given lead and lag times in the data.
1. For women diagnosed with cancer, we want to make sure that we observe a sufficient time period before diagnosis, over which there was an opportunity to screen them. So we restrict cancer diagnoses to those on 1/1/2010 and later.
1. For women screened, we want to make sure we have enough time after screening to observe cancer diagnoses, biopsy, surgery... so we restrict to screening invitations 12/31/2014 and earlier.

### Sequence of events: Breast Cancer Screening

```mermaid
graph LR

    Invite[Invited<br/>n=1,709,580<br/>n_patients=783,338]
    Screen[Screened<br/>n=594,848<br/>n_patients=364,774]
    NoScreen[Declined<br/>n=1,114,732<br/>n_patients=578,817]
    NoScreenCancer[Cancer=15,394<br/>n_patients=8,111]
    PositiveScreen[Screen Pos.<br/>n=47,013<br/>n_patients=37,806]
    NegativeScreen[Screen Neg.<br/>n=547,835<br/>n_patients=344,803]
    NegativeScreenCancer[Cancer=3,079<br/>n_patients=2,356]
    RejectS2[S2 Decline<br/>n=1,205<br/>n_patients=1,195]
    RejectS2Cancer[Cancer=15<br/>n_patients=15]
    MissingS2[S2 Missing<br/>n=902<br/>n_patients=895]
    MissingS2Cancer[Cancer=125<br/>n_patients=125]
    PositiveS2[S2 Positive<br/>n=3,518<br/>n_patients=3,483]
    NegativeS2[S2 Negative<br/>n=41,388<br/>n_patients=33,098]
    NegativeS2Cancer[Cancer=847<br/>n_patients=572]
    MissingSurgery[Surg. Missing<br/>n=946<br/>n_patients=942]
    MissingSurgeryCancer[Cancer=512<br/>n_patients=512]
    Invasive[Surg. Invasive<br/>n=2,280<br/>n_patients=2,263]
    InvasiveCancer[Cancer=1,487<br/>n_patients=1,475]
    NonInvasive[Surg. Non-invasive<br/>n=292<br/>n_patients=291]
    NonInvasiveCancer[Cancer=26<br>n_patients=26]

    Invite --> Screen
    Invite --> NoScreen
    NoScreen --> NoScreenCancer
    Screen --> PositiveScreen
    Screen --> NegativeScreen
    NegativeScreen --> NegativeScreenCancer
    PositiveScreen --> PositiveS2
    PositiveScreen --> NegativeS2
    PositiveScreen --> RejectS2
    RejectS2 --> RejectS2Cancer
    PositiveScreen --> MissingS2
    MissingS2 --> MissingS2Cancer
    NegativeS2 --> NegativeS2Cancer
    PositiveS2 --> MissingSurgery
    MissingSurgery --> MissingSurgeryCancer
    PositiveS2 --> NonInvasive
    NonInvasive --> NonInvasiveCancer
    PositiveS2 --> Invasive
    Invasive --> InvasiveCancer

    style NoScreenCancer fill:#DE9583
    style NegativeScreenCancer fill:#A3DE83
    style NegativeS2Cancer fill:#A3DE83
    style InvasiveCancer fill:#83BEDE
    style NonInvasiveCancer fill:#A3DE83
    style RejectS2Cancer fill:#A3DE83
    style MissingS2Cancer fill:#A3DE83
    style MissingSurgeryCancer fill:#A3DE83
```

1. Each observation is a screening invitation which typically occurs every 2 years for every woman between the ages of 50-69 at the time of the observation.
1. Screening Invitation: accepted or declined by patient which is indicated within the breast_screening dataset as follows:
    - Accepted: patients with non-missing screening results in the `Recall` variable as denoted below:
        - 1 - Next Round (rescreen in 2 years)
        - 2 – Early rescreen (rescreen in 1 year)
        - 3 – Mammography repetition (immediately repeat mammography)
        - 4 – Positive screening (advance to stage 2)
    - Declined: if `Recall=0` – Not Respondent (declined screening invitation)
1. Positive Screening: in the event of a positive screening, a patient advances to stage 2 which involves several tests (mammography, biopsy, ultrasound). The final results of the screening are listed in the `stage2_Rls` variable which is coded as follows:
    - 0 – Not available
    - 1 – Normal condition - rescreen at 2 years
    - 2 – Early rescreen at 1 year
    - 3 – Mammography repetition
    - 4 – Follow up at 2nd stage
    - 5 – Positive – called for 3rd stage
1. Positive Stage 2: after a positive result in stage 2 a surgery is performed and the tissue is determined to be invasive or noninvasive. The results of the surgery are listed in the `Surgery_Diagnosis` variable and coded as follows, with `≥3` coded as `invasive`:
    - 1 – Negative
    - 2 – Benign lesion
    - 3 – Micro invasive
    - 4 – In situ
    - 5 – Invasive
1. Cancer Registry: lastly, we joined the cancer registry to the screening dataset to determine how many patients of each path entered the cancer registry at some point from 2010 to 2015.

### Some observations on this process

Of all women screened after 1/1/2010 and discovered invasive tissue or were inducted into the cancer registry within one year after the screening invitation date (n_total 7,929 = registry 7,157 + invasive tissue found at surgery but not listed in cancer registry 772; for these women we would observe screening invitation for at least one year before diagnosis), 63.02% declined screening invitations
  - Median number of declined invitations before diagnosis: 1 (0-2 10-90th pctile range)
  - Median time from first declined invitation to diagnosis: 140 (43-331 10-90th pctile range)


### Risk factors (in progress)

    Specification for logit:
    dataframe={all screenings, declined screenings}
    y=appeared in cancer registry after screening
    x=all features in dem_master_lazio.feather, indicator for month of screening invitation


### Breast Screening Patient Age
First we can look at the age of women at the time of their screening invitation (this is any screening invitation, so the woman will show up potentially more than once).

- Note: this is age at screening ($`t_{screen}-t_{dob}`$), not age as noted in the original dataset

<br>
![screen ages](images/screen_ages.png)
<br>


count    1.709580e+06
mean     5.923298e+01
std      5.753939e+00
min      5.000240e+01
25%      5.416128e+01
50%      5.897725e+01
75%      6.416285e+01
max      6.999733e+01

### Screening Invitation Date
We can also look at screening exams by month and year, revealing seasonality in screening invitations and increasing time trend over years.

![screen date](images/screen_invite.png)
<br>
![screen date_year month](images/screen_invite_month.png)
<br>


count                 1986754
unique                   2227
top       2011-03-22 00:00:00
freq                     6676
first     2009-01-01 00:00:00
last      2015-12-30 00:00:00

### Breast Cancer Patient Age
Similar to screening invitations, we see that the mode of the cancer registry dataset is 50, indicating that a pool of women may be diagnosed on their first screening exam.

![cancer ages](images/cancer_ages.png)
<br>
count    10527.000000
mean        60.481619
std          6.536239
min         50.000000
25%         55.000000
50%         61.000000
75%         66.000000
max         80.000000

### Breast Cancer Patient Inclusion Date
When we look at cancer diagnoses by month and year, we see a similar seasonality to screening invitations, but lagged by 1-2 months; and the same increasing time trend over years.

<br>
![cancer date_year](images/cancer_dtinc.png)
<br>

![cancer date_year month](images/cancer_dtinc_month.png)
<br>


count                   10527
unique                   2007
top       2015-12-31 00:00:00
freq                       29
first     2010-01-02 00:00:00
last      2015-12-31 00:00:00


## 2. Data Overview

### Raw Data Files

We received 2 files:

| raw file name | description | n_rows | n_cols |
| ------------- | ----------- | ------ | ------ |
| BREAST_SCREENING.txt | patients invited to breast cancer screening | 2,165,238 | 65 |
| BREAST_CR.txt | patients diagnosed with breast cancer | 31,961 | 9 |

### Raw Variables

#### Breast Cancer - Patient Information

| Label        | Variable                        | Categories                   |
|--------------|---------------------------------|------------------------------|
| ID_P         | Personal Identifier             |                              |
| Dt_birth     | date of Birth                   |                              |
| Sex          | Sex                             |                              |
| LHU          | Local Health Unit               |                              |
| Mun_code     | Residential Municipalities_code |                              |
| Mun          | Residential Municipalities      |                              |
|              |                                 |                              |
| SSx          | Signs and syntomps              | 1 - yes; 0 – Not: <> Missing |
| HRT          | Hormonal Replacement Therapy    | 1 - yes; 0 – Not; <> Missing |
| Familiarity1 | Familiarity                     | 1 - yes; 0 – Not; <> Missing |

#### Breast Screening - Event information

| Label           | Variable                                | Categories                              |
|-----------------|-----------------------------------------|-----------------------------------------|
| ID_Event        | Event Identifier                        |                                         |
| Invitation_date | date of Screening Mammography           |                                         |
| Mammo_Read1     | Screening Mammography Reading 1         | 0 – Not available <br> 1 – Normal condition - rescreen at 2 <br> 2 – Suspicion- Early rescreen at 1 year <br> 3 – Mammography repetition <br> 4 – Positive – called for 2nd stage           |
| Mammo_Read2     | Screening Mammography Reading 2         | 0 – Not available <br> 1 – Normal condition - rescreen at 2 <br> 2 – Suspicion- Early rescreen at 1 year <br> 3 – Mammography repetition <br> 4 – Positive – called for 2nd stage           |
| Recall          | Type of Recall                          | 0 – Not Respondent <br> 1 – Next Round <br> 2 – Early rescreen <br> 3 – Mammography repetition <br> 4 – Positive screening                                                                |
| Stage2_reject   | Women decline of 2stage                 | 1 - reject                              |
| Stage2_FU       | Women with a 2nd stage exams repetition | 1 - yes                                 |

#### Breast Screening - 2nd Stage

| Label              | Variable                           | Categories                                 |
|--------------------|------------------------------------|--------------------------------------------|
| stage2_start_date  | Date of the First exam             |                                            |
| stage2_Mammo_Date  | 2nd Stage Mammography date         |                                            |
| stage2_Mammo_Rls   | 2nd Stage Mammography Results      |                                            |
| stage2_Eco_Date    | Date of 2nd Stage Ultrasound       |        |
| stage2_Eco_Rls     | 2nd Stage Ultrasound Results       |    |
| stage2_Cito_Date   | Date of 2nd Stage Citological Exam |     |
| stage2_Cito_Rls    | 2nd Stage Citological Exam Results | |
| stage2_Biopsy_Date | Date of 2nd Stage Biopsy           |       |
| stage2_Biopsy_Rls  | 2nd Stage Biopsy Results           |      |
| stage2_Rls_date    | Date of 2nd stage Final Report     | |
| stage2_Rls         | 2nd stage Final Report             | 0 – Not available <br> 1 – Normal condition - rescreen at 2 years <br> 2 – Early rescreen at 1 year <br> 3 – Mammography repetition <br> 4 – Follow up at 2nd stage <br> 5 – Positive – called for 3rd stage  |

#### Breast Screening - 3rd Stage

| Label                | Variable                    | Categories                              |
|----------------------|-----------------------------|-----------------------------------------|
| Surgery_date         | Date of surgery             |                                         |
| Surgery_procedure    | Type of procedure           |                                         |
| Surgery_N            | Number of Surgery           |         |
| Histo_date           | Date of Histological Report |    |
| Histo_Rls            | Histological Report         |  |
| ER_Pct               | Percentage of ER            |                                         |
| ER_pct_group         | Grouped ER percentage       |                                         |
| PgR_Pct              | Percentage of PgR           |                                         |
| PgR_pct_group        | Grouped PgR percentage      |                                         |
| KI67_Pct             | Percentage of KI67          |                                         |
| Size                 | Breast cancer Size          |                                         |
| Clinical_size        | Clinical_size               |                                         |
| Histological_size    | Histological_size           |                                         |
| Surgery_Results_date | Final Report stage 3_ date  |                                         |
| Surgery_Diagnosis    | Final Report stage 3        | 1 – Negative <br> 2 – Benign lesion <br> 3 – Micro invasive <br> 4 – In situ <br> 5 – Invasive    |

#### Breast Screening - 3rd Stage (Right Breast)

| Label              | Variable             | Categories                           |
|--------------------|----------------------|--------------------------------------|
| D_Diagnosis        | Diagnosis            |                                      |
| D_PTs              | PTs                  |                                      |
| D_PNs              | PNs                  |                                      |
| D_Size             | Breast cancer Size   | |
| D_Surgery          | Surgery              |                                      |
| D_ Reconstruction  | Reconstruction       |                                      |
| D_Focal            | Focal                |                                      |
| D_Margins          | Margins              |                                      |
| D_Margins_distance | Distance of Margins  |  |
| D_lymph_removal    | Lymph nodes Removed  |  |
| D_lymph_positive   | Lymph nodes positive |    |

#### Breast Screening - 3rd Stage (Left Breast)

| Label              | Variable             | Categories                           |
|--------------------|----------------------|--------------------------------------|
| S_Diagnosis        | Diagnosis            |                                      |
| S_PTs              | PTs                  |                                      |
| S_PNs              | PNs                  |                                      |
| S_Size             | Breast cancer Size   |  |
| S_Surgery          | Surgery              |                                      |
| S_ Reconstruction  | Reconstruction       |                                      |
| S_Focal            | Focal                |                                      |
| S_Margins          | Margins              |                                      |
| S_Margins_distance | Distance of Margins  |  |
| S_lymph_removal    | Lymph nodes Removed  | |
| S_lymph_positive   | Lymph nodes positive |  |

#### Processing the Dataset

First, we remove all rows without a `ID_P` and `ID_event`.
- If we use `ID_P` and `ID_event` as the global identifiers for this dataset,
we need to check that all rows have a `ID_P` and `ID_event`. Otherwise, we
can't link this to the `BREAST_CANCER.txt` file.

Referencing [SCREENING_DATASET.md](../docs/SCREENING_DATASET.md), we define the cohort as:
- women aged 50-69
    - `t0_age` defined as `Invitation_date` - `dt_birth`
- resident in Lazio Region
    - assume all patients in file are residents in Lazio Region
- invited to at least 1 screening during 2009-2015
    - subset to 2009 <= `Invitation_date` <= 2015


**Processed Data (mod)**

| mod file name | n_unique | n_var | t_earliest | t_latest | t_var | description |
| ------------- | -: | :---------: | ----------: | --------: | :-----------: | ----- |
| breast_cancer_lazio.feather | 13,535 | `ID_P` | 2009-01-01 | 2015-12-31 | `dtinc` | patients diagnosed with breast cancer |
| id_master_lazio.feather | 886,387 | `ID_P` | - | - | - | patient identifier |
| dem_lazio.feather | 886,387 | `ID_P` | - | - | - | patient screening demographics |
| screening_lazio.feather | 2,164,665 | `ID_event` | 2009-01-01 | 2015-12-30 | `Invitation_date` | mammogram screenings |
| stage2_lazio.feather  | 60,583 | `ID_event` | 2009-01-02 | 2017-12-28 | `stage2_start_date` | stage 2 results <br> {mammogram, ultrasound, citological, biopsy} |
| stage3_lazio.feather | 3,931 | `ID_event` | 2009-02-10 | 2018-04-03 | `Surgery_date` | stage 3 - surgery |




## 3. Variable summary

#### Data Files

List of data files available:
- [/data/zolab/zolab-data/lazio/data/01_master/breast_cancer_lazio.feather](#breast_cancer_lazio)
- [/data/zolab/zolab-data/lazio/data/01_master/id_master_lazio.feather](#id_master_laziofeather)
- [/data/zolab/zolab-data/lazio/data/01_master/dem_master_lazio.feather](#dem_master_laziofeather)
- [/data/zolab/zolab-data/lazio/data/01_master/screening_master_lazio.feather](#screening_master_lazio)
- [/data/zolab/zolab-data/lazio/data/01_master/stage2_master_lazio.feather](#stage2_master_laziofeather)
- [/data/zolab/zolab-data/lazio/data/01_master/stage3_master_lazio.feather](#stage3_master_laziofeather)

#### breast_cancer_lazio.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/breast_cancer_lazio.feather`

Ids:

| var        | n      | n_unique (%)    | n_missing (%)   |
|:-----------|---:|---:|---:|
| ID_P :key: | 31,916 | 13,534 (42.41%) | 18,382 (57.59%) |

Date:

| var   | n      | n_unique (%)   | n_missing (%)   | min        | mean       | max        |
|:------|---:|---:|---:|---:|---:|---:|
| dtinc | 31,916 | 2,510 (7.86%)  | 0 (0.00%)       | 2009-01-01 | 2012-08-08 | 2015-12-31 |

[back to data files list](#data-files)

Categorical (listing top 3 categories):

|                       | %      | n      | n_unique (%)   | n_missing (%)   |
|:----------------------|---:|---:|---:|---:|
| ('ICD9_code', 1749)   | 51.09% | 31,916 | 11 (0.03%)     | 0 (0.00%)       |
| ('ICD9_code', 1744)   | 23.36% |        |                |                 |
| ('ICD9_code', 1748)   | 6.79%  |        |                |                 |
| ('LHU_inc', 120102.0) | 12.84% | 31,916 | 17 (0.05%)     | 9 (0.03%)       |
| ('LHU_inc', 120103.0) | 11.67% |        |                |                 |
| ('LHU_inc', 120104.0) | 11.08% |        |                |                 |
| ('mun_inc', 58091)    | 56.08% | 31,916 | 369 (1.16%)    | 0 (0.00%)       |
| ('mun_inc', 59011)    | 2.2%   |        |                |                 |
| ('mun_inc', 58047)    | 1.25%  |        |                |                 |
| ('sede', 'MAMMELLA')  | 100.0% | 31,916 | 1 (0.00%)      | 0 (0.00%)       |
| ('sex', 2)            | 100.0% | 31,916 | 1 (0.00%)      | 0 (0.00%)       |


[back to data files list](#data-files)

#### id_master_lazio.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/id_master_lazio.feather`

Ids:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
    </tr>
    <tr>
      <th>var</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ID_P :key:</th>
      <td>2,146,118</td>
      <td>886,387 (41.30%)</td>
      <td>0 (0.00%)</td>
    </tr>
    <tr>
      <th>ID_event :key:</th>
      <td>2,146,118</td>
      <td>2,146,118 (100.00%)</td>
      <td>0 (0.00%)</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)

#### dem_master_lazio.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/dem_master_lazio.feather`

Ids:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
    </tr>
    <tr>
      <th>var</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ID_P :old_key:</th>
      <td>2,046,239</td>
      <td>851,261 (41.60%)</td>
      <td>0 (0.00%)</td>
    </tr>
    <tr>
      <th>ID_event :old_key:</th>
      <td>2,046,239</td>
      <td>2,046,239 (100.00%)</td>
      <td>0 (0.00%)</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)


Numerical:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
      <th>min</th>
      <th>mean</th>
      <th>max</th>
    </tr>
    <tr>
      <th>var</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>dt_birth</th>
      <td>2,046,239</td>
      <td>9,797 (0.48%)</td>
      <td>0 (0.00%)</td>
      <td>1939-01-21</td>
      <td>1953-06-18</td>
      <td>1965-12-18</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)


Categorical (listing top 3 categories):

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>%</th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
    </tr>
    <tr>
      <th>var</th>
      <th>value</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="2" valign="top">HRT</th>
      <th>0</th>
      <td>86.03%</td>
      <td>2,046,239</td>
      <td>2 (0.00%)</td>
      <td>1,843,984 (90.12%)</td>
    </tr>
    <tr>
      <th>1.0</th>
      <td>13.97%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">LHU</th>
      <th>RM3</th>
      <td>17.13%</td>
      <td>2,046,239</td>
      <td>10 (0.00%)</td>
      <td>87 (0.00%)</td>
    </tr>
    <tr>
      <th>RM2</th>
      <td>17.09%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>RM1</th>
      <td>15.26%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">Mun</th>
      <th>ROMA</th>
      <td>47.52%</td>
      <td>2,046,239</td>
      <td>436 (0.02%)</td>
      <td>0 (0.00%)</td>
    </tr>
    <tr>
      <th>LATINA</th>
      <td>3.17%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>VITERBO</th>
      <td>2.51%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">Mun_code</th>
      <th>58091</th>
      <td>47.52%</td>
      <td>2,046,239</td>
      <td>437 (0.02%)</td>
      <td>0 (0.00%)</td>
    </tr>
    <tr>
      <th>59011</th>
      <td>3.17%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>56059</th>
      <td>2.51%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">SSx</th>
      <th>0</th>
      <td>98.8%</td>
      <td>2,046,239</td>
      <td>2 (0.00%)</td>
      <td>1,590,482 (77.73%)</td>
    </tr>
    <tr>
      <th>1.0</th>
      <td>1.2%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">familiarity1</th>
      <th>0</th>
      <td>78.96%</td>
      <td>2,046,239</td>
      <td>2 (0.00%)</td>
      <td>1,822,750 (89.08%)</td>
    </tr>
    <tr>
      <th>1.0</th>
      <td>21.04%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>sex</th>
      <th>F</th>
      <td>100.0%</td>
      <td>2,046,239</td>
      <td>1 (0.00%)</td>
      <td>0 (0.00%)</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)

#### screening_master_lazio.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/screening_master_lazio.feather`

Ids:

| var            | n         | n_unique (%)        | n_missing (%)   |
|:---------------|---:|---:|---:|
| ID_P :key:     | 2,146,118 | 886,387 (41.30%)    | 0 (0.00%)       |
| ID_event :key: | 2,146,118 | 2,146,118 (100.00%) | 0 (0.00%)       |

Numerical:

| var             | n         | n_unique (%)   | n_missing (%)   | min        | mean       | max        |
|:----------------|---:|---:|---:|---:|---:|---:|
| Invitation_date | 2,164,871 | 2,235 (0.10%)  | 0 (0.00%)       | 2009-01-01 | 2012-09-12 | 2015-12-30 |

[back to data files list](#data-files)

Categorical (listing top 3 categories):

|                        | %      | n         | n_unique (%)   | n_missing (%)      |
|:-----------------------|---:|---:|---:|---:|
| ('Mammo_Read1', 0.0)   | 66.1%  | 2,164,871 | 5 (0.00%)      | 0 (0.00%)          |
| ('Mammo_Read1', 1.0)   | 31.56% |           |                |                    |
| ('Mammo_Read1', 4.0)   | 2.19%  |           |                |                    |
| ('Mammo_Read2', 0.0)   | 66.1%  | 2,164,871 | 5 (0.00%)      | 0 (0.00%)          |
| ('Mammo_Read2', 1.0)   | 31.67% |           |                |                    |
| ('Mammo_Read2', 4.0)   | 2.07%  |           |                |                    |
| ('Recall', 0.0)        | 66.1%  | 2,164,871 | 5 (0.00%)      | 0 (0.00%)          |
| ('Recall', 1.0)        | 31.09% |           |                |                    |
| ('Recall', 4.0)        | 2.8%   |           |                |                    |
| ('stage2_FU', 1.0)     | 100.0% | 2,164,871 | 1 (0.00%)      | 2,154,411 (99.52%) |
| ('stage2_reject', 1.0) | 100.0% | 2,164,871 | 1 (0.00%)      | 2,163,007 (99.91%) |


[back to data files list](#data-files)


#### stage2_master_lazio.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/stage2_master_lazio.feather`

Ids:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
    </tr>
    <tr>
      <th>var</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ID_P :old_key:</th>
      <td>60,627</td>
      <td>47,260 (77.95%)</td>
      <td>199 (0.33%)</td>
    </tr>
    <tr>
      <th>ID_event :old_key:</th>
      <td>60,627</td>
      <td>60,583 (99.93%)</td>
      <td>0 (0.00%)</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)


Numerical:

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
      <th>min</th>
      <th>mean</th>
      <th>max</th>
    </tr>
    <tr>
      <th>var</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>stage2_start_date</th>
      <td>60,627</td>
      <td>2,436 (4.02%)</td>
      <td>2 (0.00%)</td>
      <td>2009-01-02</td>
      <td>2013-03-13</td>
      <td>2017-12-28</td>
    </tr>
    <tr>
      <th>stage2_Mammo_Date</th>
      <td>60,627</td>
      <td>2,316 (3.82%)</td>
      <td>21,389 (35.28%)</td>
      <td>2008-05-05</td>
      <td>2013-03-31</td>
      <td>2017-12-28</td>
    </tr>
    <tr>
      <th>stage2_Eco_Date</th>
      <td>60,627</td>
      <td>2,367 (3.90%)</td>
      <td>10,433 (17.21%)</td>
      <td>2008-05-05</td>
      <td>2013-03-16</td>
      <td>2017-12-28</td>
    </tr>
    <tr>
      <th>stage2_Cito_Date</th>
      <td>60,627</td>
      <td>1,566 (2.58%)</td>
      <td>54,765 (90.33%)</td>
      <td>2008-05-05</td>
      <td>2012-05-07</td>
      <td>2017-12-04</td>
    </tr>
    <tr>
      <th>stage2_Biopsy_Date</th>
      <td>60,627</td>
      <td>1,352 (2.23%)</td>
      <td>56,884 (93.83%)</td>
      <td>2009-01-04</td>
      <td>2012-12-28</td>
      <td>2017-12-29</td>
    </tr>
    <tr>
      <th>stage2_Rsl_date</th>
      <td>60,627</td>
      <td>2,665 (4.40%)</td>
      <td>267 (0.44%)</td>
      <td>2008-05-28</td>
      <td>2013-03-18</td>
      <td>2210-01-25</td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)


Categorical (listing top 3 categories):

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>%</th>
      <th>n</th>
      <th>n_unique (%)</th>
      <th>n_missing (%)</th>
    </tr>
    <tr>
      <th>var</th>
      <th>value</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="2" valign="top">B5_type</th>
      <th>Invasivo</th>
      <td>99.51%</td>
      <td>60,627</td>
      <td>2 (0.00%)</td>
      <td>60,011 (98.98%)</td>
    </tr>
    <tr>
      <th>In Situ</th>
      <td>0.49%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">stage2_Biopsy_Rsl</th>
      <th>B2 - lesione benigna</th>
      <td>40.07%</td>
      <td>60,627</td>
      <td>6 (0.01%)</td>
      <td>56,884 (93.83%)</td>
    </tr>
    <tr>
      <th>B5 - lesione maligna per lesioni neoplastiche</th>
      <td>35.29%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>B1 - tessuto normale</th>
      <td>10.71%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">stage2_Cito_Rsl</th>
      <th>C2-reperto negativo per cellule tumorali</th>
      <td>27.6%</td>
      <td>60,627</td>
      <td>7 (0.01%)</td>
      <td>54,765 (90.33%)</td>
    </tr>
    <tr>
      <th>C5-reperto positivo per cellule tumorali maligne</th>
      <td>22.96%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>C4-reperto sospetto per lesioni neoplastiche</th>
      <td>19.17%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">stage2_Eco_Rsl</th>
      <th>U1-negativo: reperto normale</th>
      <td>59.23%</td>
      <td>60,627</td>
      <td>6 (0.01%)</td>
      <td>10,773 (17.77%)</td>
    </tr>
    <tr>
      <th>U2-benigno: lesione cistica o solida con caratteristiche di benignità</th>
      <td>28.01%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>U3-dubbio: segni dubbi con prevalenza di benignità</th>
      <td>4.56%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">stage2_Mammo_Rsl</th>
      <th>R1: negativo o quadro normale</th>
      <td>61.91%</td>
      <td>60,627</td>
      <td>6 (0.01%)</td>
      <td>21,457 (35.39%)</td>
    </tr>
    <tr>
      <th>R2: benigno</th>
      <td>18.95%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>R3: probabilmente benigno\t</th>
      <td>8.22%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th rowspan="3" valign="top">stage2_Rls</th>
      <th>1.0</th>
      <td>54.84%</td>
      <td>60,627</td>
      <td>6 (0.01%)</td>
      <td>0 (0.00%)</td>
    </tr>
    <tr>
      <th>4.0</th>
      <td>21.05%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>2.0</th>
      <td>12.27%</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

[back to data files list](#data-files)

#### stage3_lazio_master.feather
File: `/data/zolab/zolab-data/lazio/data/01_master/stage3_lazio_master.feather`

Ids:

| var                | n     | n_unique (%)   | n_missing (%)   |
|:-------------------|---:|---:|---:|
| ID_P :old_key:     | 3,974 | 3,912 (98.44%) | 11 (0.28%)      |
| ID_event :old_key: | 3,974 | 3,930 (98.89%) | 0 (0.00%)       |

Date:

| var              | n     | n_unique (%)   | n_missing (%)   | min        | mean       | max        |
|:-----------------|---:|---:|---:|---:|---:|---:|
| Surgery_date     | 3,974 | 1,476 (37.14%) | 520 (13.09%)    | 2009-02-10 | 2013-04-14 | 2018-04-03 |
| Histo_date       | 3,974 | 1,484 (37.34%) | 1,021 (25.69%)  | 2005-03-10 | 2013-06-19 | 2018-03-21 |
| Surgery_Rls_date | 3,974 | 1,435 (36.11%) | 310 (7.80%)     | 2009-02-10 | 2013-11-07 | 2018-03-21 |

[back to data files list](#data-files)

Categorical (listing top 3 categories):

|                                               | %      | n     | n_unique (%)   | n_missing (%)   |
|:----------------------------------------------|---:|---:|---:|---:|
| ('D_Diagnosis', '-')                          | 44.84% | 3,974 | 28 (0.70%)     | 1,068 (26.87%)  |
| ('D_Diagnosis', 'Ca.duttale infiltrante')     | 31.31% |       |                |                 |
| ('D_Diagnosis', 'Ca.Duttale in situ')         | 3.75%  |       |                |                 |
| ('D_Reconstruction', 'Effettuata')            | 100.0% | 3,974 | 1 (0.03%)      | 3,825 (96.25%)  |
| ('D_Surgery', 'Dato non disponibile')         | 47.81% | 3,974 | 11 (0.28%)     | 393 (9.89%)     |
| ('D_Surgery', 'Quadrantectomia')              | 36.58% |       |                |                 |
| ('D_Surgery', 'Mastectomia Totale')           | 3.83%  |       |                |                 |
| ('D_lymph_positive', 0.0)                     | 97.21% | 3,974 | 7 (0.18%)      | 1,539 (38.73%)  |
| ('D_lymph_positive', 1.0)                     | 2.18%  |       |                |                 |
| ('D_lymph_positive', 2.0)                     | 0.25%  |       |                |                 |
| ('D_lymph_removal', 0.0)                      | 84.02% | 3,974 | 16 (0.40%)     | 1,539 (38.73%)  |
| ('D_lymph_removal', 1.0)                      | 9.12%  |       |                |                 |
| ('D_lymph_removal', 2.0)                      | 3.53%  |       |                |                 |
| ('ER_pct_group', 'N.D.')                      | 51.4%  | 3,974 | 8 (0.20%)      | 3,404 (85.66%)  |
| ('ER_pct_group', '75+%')                      | 31.75% |       |                |                 |
| ('ER_pct_group', 'Positivo')                  | 6.67%  |       |                |                 |
| ('Histo_results', 'Ca.duttale infiltrante')   | 57.51% | 3,974 | 28 (0.70%)     | 1,298 (32.66%)  |
| ('Histo_results', 'Ca.Duttale in situ')       | 7.47%  |       |                |                 |
| ('Histo_results', 'Ca.lobulare infiltrante')  | 6.76%  |       |                |                 |
| ('PgR_pct_group', '75+%')                     | 28.93% | 3,974 | 9 (0.23%)      | 3,694 (92.95%)  |
| ('PgR_pct_group', '50-74%')                   | 15.71% |       |                |                 |
| ('PgR_pct_group', '1-24%')                    | 12.5%  |       |                |                 |
| ('S_Diagnosis', '-')                          | 43.47% | 3,974 | 27 (0.68%)     | 1,009 (25.39%)  |
| ('S_Diagnosis', 'Ca.duttale infiltrante')     | 28.94% |       |                |                 |
| ('S_Diagnosis', 'Ca. infiltrante N.C.')       | 6.1%   |       |                |                 |
| ('S_Reconstruction', 'Effettuata')            | 100.0% | 3,974 | 1 (0.03%)      | 3,820 (96.12%)  |
| ('S_Surgery', 'Dato non disponibile')         | 49.03% | 3,974 | 12 (0.30%)     | 511 (12.86%)    |
| ('S_Surgery', 'Quadrantectomia')              | 35.84% |       |                |                 |
| ('S_Surgery', 'Mastectomia Totale')           | 3.52%  |       |                |                 |
| ('S_lymph_positive', 0.0)                     | 96.6%  | 3,974 | 7 (0.18%)      | 1,706 (42.93%)  |
| ('S_lymph_positive', 1.0)                     | 2.47%  |       |                |                 |
| ('S_lymph_positive', 2.0)                     | 0.66%  |       |                |                 |
| ('S_lymph_removal', 0.0)                      | 81.79% | 3,974 | 14 (0.35%)     | 1,706 (42.93%)  |
| ('S_lymph_removal', 1.0)                      | 9.57%  |       |                |                 |
| ('S_lymph_removal', 2.0)                      | 4.89%  |       |                |                 |
| ('Surgery_Diagnosis', 5.0)                    | 75.0%  | 3,974 | 6 (0.15%)      | 510 (12.83%)    |
| ('Surgery_Diagnosis', 2.0)                    | 8.8%   |       |                |                 |
| ('Surgery_Diagnosis', 4.0)                    | 8.2%   |       |                |                 |
| ('Surgery_procedure', 'Quadrantectomia')      | 66.11% | 3,974 | 12 (0.30%)     | 761 (19.15%)    |
| ('Surgery_procedure', 'Dato non disponibile') | 9.03%  |       |                |                 |
| ('Surgery_procedure', 'Mastectomia Totale')   | 6.35%  |       |                |                 |
