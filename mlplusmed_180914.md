# Getting started in data science and medicine


[Ziad Obermeyer](http://sph.berkeley.edu/ziad-obermeyer-md), UC Berkeley

August 2018


&nbsp;

Some of the most interesting research in medicine today is growing in an insterstitial space between several fields -- clinical practice, biology and physiology, computer science, economics and statistics, and even human psychology. This work still has a 'frontier' feel: many disciplines are trying to stake a claim to this territory, but it's clear that no single one can have a monopoly on the methods and content needed; no existing PhD program will teach it all. That makes it exciting, but it also makes it hard to know where to start.

I've tried to sketch out a tentative answer to the question, *'What do I need to do this work well?'* It's subjective and incomplete, and drawn largely from my own experience in learning how to work in this area. But I hope it's a useful list of some of the puzzle pieces.


## Motivation

Data, as we’ve all heard, will transform medicine. There are many perspective-style pieces on this point (e.g.. [Obermeyer and Emanuel 2015](https://catalyst.nejm.org/big-data-machine-learning-clinical-medicine/), [Obermeyer and Lee 2017](https://www.nejm.org/doi/10.1056/NEJMp1705348)), but more importantly, there are concrete examples:
- Machines are *interpreting complex medical data* at performance levels equal to human doctors.
	- ECGs: [Rajpurkar et al. 2017](https://arxiv.org/pdf/1707.01836.pdf)
	- Chest x-rays: [Rajpurkar et al. 2017](https://arxiv.org/pdf/1711.05225.pdf)
	- Moles: [Esteva et al. 2017](https://www.nature.com/articles/nature21056)
	- Retinae: [Gulshan et al. 2016](https://jamanetwork.com/journals/jama/fullarticle/2588763)
- They are *predicting clinical outcomes* in a way that could help human doctors make decisions.
	- Sepsis: [Henry et al. 2015](http://stm.sciencemag.org/content/7/299/299ra122.short)
	- Rehospitalization in hospitalized patients: [Rajkomar et al. 2018](https://arxiv.org/pdf/1801.07860.pdf)
	- 30-day mortality after starting chemotherapy: [Elfiky et al. 2018](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2688539)
- They are helping us understand important issues in *health policy*.
	- End of life spending: [Einav et al. 2018](http://science.sciencemag.org/content/360/6396/1462.long)
	- Health plan payment models: [Bergquist et al. 2018](http://www.nber.org/papers/w24491)
- They may even be making *new medical discoveries* previously unsuspected by humans.
	- New pathological features associated with metastasis: [Beck et al. 2011](http://med.stanford.edu/labs/vanderijn-west/documents/108ra113.full.pdf)
	- Microscopic ECG variability associated with sudden death: [Chia and Syed 2014](https://dl.acm.org/citation.cfm?id=2623702)
	- New uses for old drugs: [Dudley et al. 2011](http://dudleylab.org/wp-content/uploads/2016/02/Computational-repositioning-of-the-anticonvulsant-topiramate-for-inflammatory-bowel-disease..pdf)
	- Using the microbiome to predict glucose spikes and optimize diet: [Zeevi et al. 2015](https://www.sciencedirect.com/science/article/pii/S0092867415014816)


## Some areas to get good at
To do this work well, several skills are required. Some are obvious, but others are less so.

### Basic empirical data skills
Medical data are the product of a messy combination of human behavior, social systems, and biology. Doing research with these data raises a set of challenges that look a lot like social science problems.

A nice corollary of this is that many of the critical issues that come up often -- measurement error, sample selection, etc -- have been thoroughly mapped out in other fields, that have a long history of making sense of data from social systems:  economics, statistics, and political science, etc.

##### There are many textbooks that will teach you about these things.
I am partial to econometric textbooks because I find them to be the most clearly written for my style of thinking. But many will accomplish this goal (advice: pick one; work through the p-sets).
- My favorite is [Ashenfelter, Levine, and Zimmerman](https://www.amazon.com/Statistics-Econometrics-Applications-Orley-Ashenfelter/dp/0471107875/ref=sr_1_1?s=books&ie=UTF8&qid=1533676977&sr=1-1&keywords=Statistics+and+Econometrics%3A+Methods+and+Applications)
- A very common choice is [Wooldridge](https://www.amazon.com/Introductory-Econometrics-Modern-Approach-MindTap/dp/130527010X/ref=sr_1_1?s=books&ie=UTF8&qid=1533677002&sr=1-1&keywords=wooldridge+introductory+econometrics), which is also great

##### Why this is important
There are a growing number of examples of how subtle statistical issues can lead to major problems for predictive algorithms.
- Selective labels (a cousin of potential outcomes, $`y_1`$ vs. $`y_0`$): [Lakkaraju et al. 2017](http://www.kdd.org/kdd2017/papers/view/the-selective-labels-problem-evaluating-algorithmic-predictions-in-the-pres) outline the problem, and [Lakkaraju et al. 2018](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5947971/) is a fully worked-through example from another field with direct applications to medicine (judges == doctors).
- Measurement error ($`y`$ vs. $`y^*`$): [Mullainathan and Obermeyer 2017](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5540263/)

I can also give a more positive example, of how old statistics + new data can be powerful: our own [paper on body temperature](https://www.bmj.com/content/359/bmj.j5468.long) from 2017.
- There is no machine learning here, just random effects regression.
- But it uncovered some interesting new facts about body temperature: individual patients' have temperature baselines that are very different from the average body temperature we are taught in medical school, and these personalized temperatures are surprisingly strongly correlated with mortality.
- The article is now cited (it gets a whole paragraph!) -- in [UpToDate's article on fever](https://www.uptodate.com/contents/pathophysiology-and-treatment-of-fever-in-adults?search=fever&source=search_result&selectedTitle=4~150&usage_type=default&display_rank=4), which is the resource most doctors use as their primary medical reference text.


### Causal inference
There is a growing amount of cutting-edge work at the interface of machine learning and causal inference (e.g., [van der Laan and Rose 2011 & 18](http://www.targetedlearningbook.com/), [Wager and Athey 2017](https://arxiv.org/pdf/1510.04342.pdf), [Belloni, Chernozhukov, & Hansen](https://www.aeaweb.org/articles?id=10.1257/jep.28.2.29)).

But this advanced toolkit is not needed for the vast majority of problems that come up in machine learning projects. What is needed is the '101' level skills to solve some very standard causal inference problems.

This is because most 'prediction problems' are not pure prediction, but *also* involve some aspect of causation, as laid out in this [Ludwig et al. 2015 article on prediction vs. causation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4869349/). For example, to understand the potential impact of an algorithm in practice using data you already have, you might look for natural experiments, e.g., as-if-random assignment of patients to doctors with different practice patterns. This can help you estimate the scope for improvement from algorithmic predictions.

Angrist and Pischke have written two outstanding, accessible books (advice: pick one; work through everything).
- The standard choice: [Mostly Harmless](http://www.mostlyharmlesseconometrics.com/)
- A bit less math: [Mastering 'Metrics](http://masteringmetrics.com/)


### Machine learning

##### How to think about what machine learning does and does not do
I've found two overview pieces to be extremely helpful. They are nominally focused on economic applications of machine learning, but are sufficiently broad to be good summaries of its usefulness in general.
- [Athey 2018](http://www.nber.org/chapters/c14009.pdf), or Imbens and Athey's [NBER lectures on machine learning](http://www.nber.org/econometrics_minicourse_2015/)
- [Mullainathan and Spiess 2017](https://pubs.aeaweb.org/doi/pdfplus/10.1257/jep.31.2.87)

The article cited above (prediction vs. causation) also makes the case that 'prediction policy problems' are widespread, interesting, and important in policy: [Ludwig et al. 2015](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4869349/)

##### Mechanics of machine learning
One way to think about machine learning is as a large toolkit of functions for fitting $`y=\hat{f}(X)`$. Of course, machine learning functions are different from regression (e.g., they are far more expressive and complex; they can handle extremely high-dimensional data, like images or waveforms, that a regression framework could not; they can do cool tricks, like fit functions even when $`k>n`$ [i.e., more variables than observations]). But if you know how to use regression -- and where regression goes wrong -- you'll be in a great position to start applying machine learning, when and if it's needed.

The basics of regression are covered in materials noted above in [Causal inference](#causal-inference).

Building on regression, you can learn about the mechanics of the machine learning toolkit a few different ways:
- The canonical machine learning text:
	- [Elements of Statistical Learning](https://web.stanford.edu/~hastie/ElemStatLearn/)
	- Or a version with a bit less math: [Introduction to Statistical Learning](http://www-bcf.usc.edu/~gareth/ISL/)
- I personally like [Machine learning: a probabilistic perspective](https://mitpress.mit.edu/books/machine-learning-1). It works from a principled probabilistic framework rather than just presenting a lot of different algorithms, and is well and accessibly written.

##### Staying current
Of course, machine learning is an exciting and dynamic field to be working in, and a lot of progress happens in conference proceedings and R or Python packages rather than textbooks. That's what makes it fun, but it's also what can make it confusing and hard to get a handle on what's going on.

One way to deal with this is to just keep reading, going to talks, and connecting with people doing cool stuff. Some of the papers cited [above](#Motivation) distill the [newer neural network architectures](https://arxiv.org/pdf/1707.01836.pdf) or [creative ways to 'embed' data in low-dimensional representations](https://arxiv.org/pdf/1801.07860.pdf), etc.

##### A few cool non-medical papers with relevance to medicine and public health
This list is a scattered list of things that have caught my attention -- please feel free to suggest others.
- Measuring poverty in developing countries using mobile phone data ([Blumenstock, Cadamuro, and On 2016](http://jblumenstock.com/files/papers/jblumenstock_2015_science.pdf)) or satellite imagery ([Jean et al. 2016](https://europepmc.org/abstract/med/27540167))
- Estimating neighborhood characteristics using Google Street View images ([Gebru et al. 2017](http://www.pnas.org/content/early/2017/11/27/1700035114.full))
- Looking for child maltreatment using Google search ([Stephens-Davidowitz 2013](http://sethsd.com/s/childabusepaper13.pdf))
- Predicting restaurant hygiene scores with Yelp reviews ([Kang et al. 2013](http://www.aclweb.org/anthology/D13-1150))
- Improving on judges' bail decisions ([Lakkaraju et al. 2018](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5947971/))
- Matching refugees to regions where they are most likely to be employed ([Bansak et al. 2018](http://science.sciencemag.org/content/359/6373/325))
- Discovering new emerging topics in scientific journals ([Griffiths and Steyvers 2004](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC387300/))
- Predicting plasma instability in fusion reactors to avoid damage ([Kates-Harbeck, Svyatkovskiy &  Tang 2019](https://www.nature.com/articles/s41586-019-1116-4))
- Sifting through data from the Event Horizon telescope to represent a black hole ([Atlantic 2019](https://www.theatlantic.com/science/archive/2019/04/black-hole-event-horizon-telescope/586846/))  

### Medicine

There's 'Statistics for Dummies' and 'Machine Learning for Dummies.' Why isn't there 'Medicine for Dummies'?

Medicine, as it's currently taught and practiced, is more of a collection of facts than a coherent field with unified theories. Of course, a lot of other fields intersect with medicine: genetics, biology and biochemistry, epidemiology, statistics, economics, ... But if you take out all those intersections, there's still a lot left: that's medicine, and it seems kind of important. But what is it and how do you learn about it?

There are a few subfields of biology and medicine that are oriented to principles and theories rather than cookbook-like catalogues of facts and heuristics.
1. Physiology and pathophysiology
	- Physiology is the study of how the body functions under normal conditions. There are some good [overarching principles](https://www.physiology.org/doi/full/10.1152/advan.90139.2008) here (e.g., homeostasis and control mechanisms, information flow and signaling, thermodynamics of matter-energy transformations, complexity and organization of tissues from basic building blocks, etc.). There are many, many physiology textbooks that all teach approximately the same material. A few favorites are
		- Stanfield's [Principles of Human Physiology](https://www.amazon.com/gp/product/1292156481/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
		- The more standard [Guyton and Hall](https://www.amazon.com/gp/product/1455770051/ref=ox_sc_sfl_title_2?ie=UTF8&psc=1&smid=ATVPDKIKX0DER) is also very good.
		- As is Fox's [Human Physiology](https://www.amazon.com/gp/product/1259060543/ref=ox_sc_sfl_title_1?ie=UTF8&psc=1&smid=A30UH7RV1UCIV3)
	- Pathology is the study of how normal function breaks down. You won't find much on cancer or inflammation in physiology books; that's here.
		- The classic text is [Robbins and Cotran](https://www.amazon.com/Robbins-Cotran-Pathologic-Disease-Pathology/dp/1455726133/ref=sr_1_1?s=books&ie=UTF8&qid=1534131464&sr=1-1), or the [simpler version](https://www.amazon.com/Robbins-Basic-Pathology/dp/0323353177/ref=dp_ob_title_bk). I haven't found a good alternative.
- Artificial organ models. Some activities in medicine involve taking over or replacing the normal function of specific organ systems. This of course forces people to understand in very specific ways the inputs and outputs of a system, well enough to replicate it. Of course, the artificial system mimics rather than replicates the organ system -- the dialysis machine is not a kidney; the airplane is not a bird -- but the engineering principles of the designed system can give insights into the biological one.   
	- Anesthesia and critical care: During procedures in the operating room, or often in the ICU, consciousness and voluntary muscle functions are suspended pharmacologically, and machines like ventilators take over. So anesthesia textbooks end up being very good physiology textbooks. A couple to glance at:
		- [Basic Science for Anaesthetists](https://www.cambridge.org/core/books/basic-science-for-anaesthetists/D3FFB9F24BA3AD1EE14DF386927CF037)
		- [The ICU Book](https://www.amazon.com/Marinos-ICU-Book-Updates-Marino/dp/1451121180)
	- Some other things like this (that you wouldn't want to read a book about, but might do a quick search on to figure out how they work)
		-	Hemodialysis (==kidney)
		- Cardiopulmonary bypass pump (==heart)
		- Ventricular Assist Device (VAD, also == heart)
		- Mechanical ventilation (==breathing mechanics; note this is not a lung! people on a ventilator still depend on their lungs to exchange gases, the machine is *only* doing the mechanical work of breathing)
		- Extracorporeal Membrane Oxygenation (ECMO, == heart + lung)
		- Total Parenteral Nutrition (TPN; == food injections for those without functioning intestines)
1. Pharmacology is another subfield with well-defined principles and methods, and with an outstanding textbook by [Golan](https://www.amazon.com/gp/product/1451191006/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1).
